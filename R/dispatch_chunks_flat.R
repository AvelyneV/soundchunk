#' Dispatch Chunks into Flat Database (`flat_database` in `dispatch_chunks`)
#'
#' @inherit dispatch_chunks description details params note
#'
#' @seealso [dispatch_chunks_flat_file()], [dispatch_chunks()]
#' @noRd
dispatch_chunks_flat <- function(src_path,
                                 dst_path,
                                 channel = NULL,
                                 security,
                                 lab_ext,
                                 parameters_file) {

  ## sanity check
  if (!is_valid_channel(channel)) {
    stop("I stop - invalid channel")
  }

  if (!dir.exists(dst_path)) {
    dir.create(dst_path, recursive = TRUE)
    print(glue("{dst_path} created."))
  } else {
    if (length(dir(dst_path)) != 0) {
      stop(glue("{dst_path} is not empty.
                I don't want to empty it myself. Please empty or delete it."))
    }
  }

  ## load naming rule
  load_naming_rule()

  ## keep info SHOW_STEPS
  show_steps_tmp <- show_steps()

  ## clean lab_ext
  lab_ext <- clear_front_dot(lab_ext)

  ## list all the files in the directory
  files <- list.files(file.path(src_path), pattern = glue("\\.{lab_ext}$"))

  ## initialize
  first_file <- TRUE
  flat_dir_parameters <- data.frame()
  tryCatch(
    {
      ## go through the files
      for (file in files) {
        ##### PLAY FUNCTION DISPATCH_CHUNKS_FLAT_FILE
        if (first_file && show_steps()) {
          print("Showing steps on first file")
          pause()
        }
        flat_file_parameters <- dispatch_chunks_flat_file(
          src_path,
          label_file = file,
          channel = channel,
          dst_path,
          security,
          lab_ext,
          parameters_file,
          is_rule_loaded = TRUE
        )

        if (first_file && show_steps()) {
          catn("First file is done. Mode SHOW_STEPS = FALSE for next files")
          pause()
        }

        set_show_steps(FALSE)
        first_file <- FALSE

        ## update flat_database_parameters
        flat_dir_parameters <- rbind(
          flat_dir_parameters,
          flat_file_parameters
        )
      }
    },
    error = function(cond) {
      catn("ALERT ERROR!")
      catn("dispatch_chunks_flat_file did not run - program stopped")
      message(cond)
      pause()
    },
    finally = {
      set_show_steps(show_steps_tmp)
    }
  )

  ## name of database parameter file
  naming_rule <- .scnf$NAMING_RULE
  separator <- naming_rule$separator # nolint
  prefix <- naming_rule$prefix

  if (prefix == "") {
    flat_parameters_file_name <- glue("parameters.tsv")
  } else {
    flat_parameters_file_name <- glue("{prefix}{separator}parameters.tsv")
  }
  flat_parameters_file <- file.path(dst_path, flat_parameters_file_name)

  write.table(flat_dir_parameters, flat_parameters_file,
    sep = "\t", row.names = FALSE, quote = FALSE
  )
  catn(glue("Parameters written in {flat_parameters_file}"))

  return(flat_dir_parameters)
}



#' Dispatch Chunks of One file into Flat Database
#'
#' Read all rows of `lab_ext` file, get start and end of the chunk (according to
#' `security`), create a proper filename for the chunks based on the loaded
#' naming rule, extract the chunks and save the audio files in `dst_path`,
#' monitor the extraction in a dataframe log saved as `.tsv` file in `dst_path`.
#'
#' @details
#' How the path to a created chunk is built:
#' - the rule that is used to build the filename
#' can be set through [set_naming_rule()] (interactive) or
#' [set_naming_rule_path()] (if you already stored a naming rule)
#' - for a given chunk, we look for the values of the parameters that appear
#' in the naming rule:
#'   - in the `parameters_file` (that describes the context of recording)
#'   - in the label track file (that describes the chunks in their recording)
#' - the name of the file is built from both `group` and `chunk` entries of the
#' naming rule
#' - both entries are associated with a vector, and the values of their elements
#' are joined by the `separator` entry to generate the chunk filename
#'
#' For each chunk in the label track file:
#' - we get the start and end of each labelled chunk,
#' - we build the chunk file name,
#' - we create the subdirectory if needed,
#' - we apply the `security` to extract the chunk and save the wave file,
#' - we monitor the extraction and save it to `full_database_parameters.tsv`
#' in `dst_path`
#'
#' @inherit dispatch_chunks params note
#'
#' @param label_file `chr` -- the name the label track
#' @param is_rule_loaded `boolean` -- is the naming rule already loaded
#'
#' @return `dataframe` containing both the context of recording and the
#' parameters of the chunks
#'
#' @export
#'
dispatch_chunks_flat_file <- function(src_path,
                                      label_file,
                                      dst_path,
                                      channel = NULL,
                                      security,
                                      lab_ext,
                                      parameters_file,
                                      is_rule_loaded = FALSE) {

  if (!is_rule_loaded) {
    load_naming_rule()
  }

  ## sanity check
  if (!is_valid_channel(channel)) {
    stop("I stop")
  }

  ## clear and get extensions
  lab_ext <- clear_front_dot(lab_ext)
  wav_ext <- get_wav_ext() # nolint

  print(glue("Dispatching file: {label_file}"))

  ## get the file name of the corresponding wav file
  origin <- stri_sub(label_file, 1, -(nchar(lab_ext) + 2))
  wav_file <- glue("{origin}.{wav_ext}")

  ## load parameters
  parameters <- read.table(parameters_file, header = TRUE, sep = "\t")

  labels <- read_like_audacity(file.path(src_path, label_file))

  ## continue only if some chunks would be extracted in this file
  if (nrow(labels) == 0) {
    if (show_steps()) {
      print("Label track is empty - empty dataframe returned")
      pause()
    }
    return(data.frame())
  }

  if (show_steps()) {
    catn("Below the label track:")
    show(labels)
    pause()
  }

  wave_headers = readWave(file.path(src_path, wav_file),
           header = TRUE
  )

  ## test if wav file exist before starting extraction process
  if (!file.exists(file.path(src_path, wav_file))) {
    print("ALERT")
    print(glue(
      "It looks like the wav file: {wav_file} ",
      "in the recording directory: {src_path} ",
      "does not exist. File will be skipped."
    ))
    pause()

    return(data.frame())
  }

  ## look for the structure of the future file names containing all information
  ## contained in the rule_json file.

  if (show_steps()) {
    print("You chose to build: flat_database")
    pause()
  }

  ## for each labelled chunk:
  ## - build the chunk wave file name
  ## - extract with security
  ## - save
  for (i in seq_len(nrow(labels))) {
    ## gather info of parameters.tsv and tab to create file name
    parameters_rec <- parameters[parameters$origin == origin, ]
    if (nrow(parameters_rec) == 0 || length(parameters_rec) == 0) {
      print("ALERT")
      print(glue(
        "Problem reading the parameters_file:
        {parameters_file}.
        I stop.
        hints: Does the file contain an 'origin' column?
               Does it have more than one colum?
        "
      ))
      pause()
      stop()
    }

    tab_chunk <- cbind(labels[i, ], parameters_rec, row.names = NULL)

    chunks_origin <- instantiate_group_chunk(tab_chunk)
    tab_chunk <- cbind(chunks_origin, tab_chunk)

    chunk_wav_filename <- glue("{chunks_origin}.{wav_ext}")
    chunk_file <- file.path(dst_path, chunk_wav_filename)

    ## extract and save chunk
    s <- as.double(labels$start[i]) - security
    e <- as.double(labels$end[i]) + security
    if (wave_headers$sample.rate * s > wave_headers$samples || e < 0) {
      next
    }
    sound <- readWave(file.path(src_path, wav_file),
      from = s, to = e, units = "seconds"
    )

    ## export according to channel : left, right,
    if (!is.null(channel)) {
      if (channel == "right" && !(sound@stereo)) {
        stop("You asked me to read the right channel of a mono file.")
      }
      sound <- mono(sound, which = channel)
    }
    writeWave(sound, chunk_file)

    ## keep extraction info in a dataframe
    if (i == 1) {
      flat_database_param_f <- tab_chunk
    } else {
      flat_database_param_f <- rbind(flat_database_param_f, tab_chunk)
    }

    if (show_steps()) {
      print(glue("Name of new chunk: {chunks_origin}"))
    }
  }
  return(flat_database_param_f)
}

