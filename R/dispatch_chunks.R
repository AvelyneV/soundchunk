#' Dispatch Chunks
#'
#' Generate chunk databases of different types.
#' Run on all label track file (*i.e.* files with `lab_ext` extension) in the
#' `src_path` directory. For each label track/audio file of the directory run
#' either [dispatch_chunks_flat_file()] or [dispatch_chunks_grouped_file()]
#' based on `flat_database`. See details
#'
#' @details
#' If `flat_database = FALSE`.
#' Create as many subdirectories in `dst_path` as the number of
#' factors of `group` key of the `naming_rule` (fed with the contexts of the
#' recordings and the chunk info in the label tracks), and a log dataframe
#' in every subdirectory.
#' For each chunk in each `lab_ext` label track file of `src_path`:
#' 1. get start and end of the chunk,
#' 2. build a filename, according the `naming_rule`
#'    (`chunk` key), fed with the context of recording (in `parameters_file`)
#'    and the chunk info (columns of the source label track)
#' 3. apply the `security` to extract the chunks and save the audio file
#'    in the corresponding subdirectory of `dst_path`
#' 4. monitor the extraction in a log dataframe, append it to the `.tsv` file
#'    of the corresponding sub directory of the `dst_path`
#'
#' If `flat_database = TRUE`.
#' For each chunk in each `lab_ext` label track file of `src_path`:
#' 1. get start and end of each labelled chunk,
#' 2. build a filename, according the `naming_rule`
#'    (`chunk` key), fed with the context of recording (in `parameters_file`)
#'    and the chunk info (columns of the source label track)
#' 3. apply the `security` to extract the chunks and save the audio file in the
#' `dst_path` directory,
#' 4. monitor the extraction in a dataframe and append it to the
#' `flat_database_parameters.tsv` of `dst_path`
#'
#' @param src_path `chr` -- absolute path to the directory that
#'        contains the audio file(s) and the label track(s) with the chunks to
#'        dispatch
#' @param dst_path `chr` -- absolute path to the future database directory
#' @param channel `chr` (`left` or `right` or `NULL`) -- channel(s) of interest
#' @param security `numerical` (in s) -- the label boundaries are expanded by
#'        `security` seconds both sides before chunk extraction
#' @param lab_ext `chr` -- the extension of the label track file(s)
#' @param flat_database `boolean` -- If `TRUE`, the function
#'        `dispatch_chunks_flat` is run and all chunks are saved in the same
#'        directory. If `FALSE`, the function `dispatch_chunks_grouped` is run
#'        and the chunks are dispatched in several directories, according to the
#'        naming rule
#' @param parameters_file `chr` -- absolute path to a parameters file,
#'        containing the context of the recordings. Used to decide for the final
#'        names of dispatched chunks. Tab separated file.
#'
#'
#'
#' @note
#' Pre-requisite:
#' - having `lab_ext` file(s) *WITH HEADERS* containing as many rows as the
#'   number of labelled chunks we want to extract and at least three columns:
#'   `start`, `end` and info of each chunk (*e.g.* `label`).
#'   See [clean_audacity()]
#' - the basename (*i.e.* without extension) of the `lab_ext` files should
#'   be the same name as the basename of the corresponding audio file
#' - these `lab_ext` files should be stored in the same directory as the audio
#'   files
#' - these `lab_ext` files should contain the columns used by the `naming_rule`
#' if not contained in the `parameters file`.
#'
#' If `channel` is `left` or `right`, the created chunks are mono files based on
#' the left or right channel of the input audio file. If `channel` is `NULL` and
#' the input audio file is stereo, the created chunks are stereo files.
#'
#' @export
#' @seealso [set_naming_rule()], [set_naming_rule_path()].
#'
dispatch_chunks <- function(src_path, dst_path,
                            channel = NULL,
                            lab_ext,
                            security = 0,
                            flat_database = TRUE,
                            parameters_file = file.path(src_path, 'parameters.tsv')) {

  ## sanity checks
  if (!is.null(channel) && channel != "left" && channel != "right") {
    stop(glue("The channel argument should be either NULL (default), ",
              "'left' or 'right'"))
  }

  if (!file.exists(parameters_file)) {
    catn(glue("could not find a parameters.tsv file in {src_path}"))
    stop("You should provide a tab separated values file, matching the name of the audio files to experimental conditions. One of the column should be 'origin'= name of audio file without extension")
  }

  load_naming_rule()
  print_pkg_environment(FALSE, TRUE, TRUE, TRUE, FALSE, FALSE)

  if (interactive()) {
    keep_going <- prompt_yn("Are you ok with these settings?")
    if (!keep_going) {
      return()
    }
  }

 ## play the right dispatch function
  if (flat_database) {
    database_parameters <- dispatch_chunks_flat(
      src_path,
      dst_path,
      channel = channel,
      security,
      lab_ext,
      parameters_file
    )
    return(database_parameters)
  } else {
    dispatch_chunks_grouped(
      src_path,
      dst_path,
      channel = channel,
      security,
      lab_ext,
      parameters_file
    )
  }
}
