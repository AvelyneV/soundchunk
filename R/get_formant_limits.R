#' Find expected formants limits in chunk
#'
#' Compute the meanspec of `chunk`, look for `nb_formants` peak in menspecs and
#' define formants boundaries
#'
#' @param chunk -- a wave object
#' @param origin `chr` -- the base name of the wave object (no extension)
#' @param nb_formants `num` -- number of expected formants
#' @param formants_bw -- formants bandwidth, a vector of length 2
#'                       with minimum and maximum frequency of expected
#'                       formants (Hz). Default is c(0,10000)
#' @param fft_formants -- a vector of length 2 with FFT window length (points)
#'                                  and overlap between windows (%).
#'                                  Default is c(256,50).
#'
#' @details
#' Uses meanspec() function of Seewave package with `wl = fft_wl_formants` and
#' `ov = fft_ov_formants`, between min and maximum frequencies defined by `formants_bw`.
#' Uses fpeaks() function of Seewave package on meanspec using
#' `nmax = nb_formant` between min and maximum frequencies defined by
#' `formants_bw`.
#'
#' @return
#' A list of two elements: a one row dataframe with the boundaries of each expected
#' formants and a dataframe log of failed chunks and error type
#' @export
#'
#' @seealso [get_formants_limits_file()][get_formants_limits_dir()]
get_formants_limits_chunk <- function(chunk, origin, nb_formants,
                                      formants_bw = c(0,10000),
                                      fft_formants = c(256,50)) {


  #####-------------
  ## CONTROLS

  catn(glue('Searching formants limits in sound: {origin}'))

  ## deal with frequency bandwidth for detection
  if (!is.vector(formants_bw) || !is.numeric(formants_bw)) {
    stop(glue("formants_bw={formants_bw} should be a numeric vector of length 2:
              min and max frequency in Hz, e.g. freq = c(150,10000)"))
  } else {
    min_freq_formants <- formants_bw[1]
    max_freq_formants <- formants_bw[2]
    if (max_freq_formants < min_freq_formants){
      stop(glue("max formants frequency={max_freq_formants} should be greater
                than min formants frequency={min_freq_formants}"))
    }
  }

  if (nb_formants < 1) {
    stop(glue("Excuse me, but you expect {nb_formants} formants??
              Sorry, I cannot please you."))
  }

  ## deal with fft_formants
  if (!is.vector(fft_formants)||!is.numeric(fft_formants)) {
    stop(glue("fft_formants should be a numeric vector of length 2: c(fft_wl_formants, fft_ov_formants) (e.g. fft_analysis = c(512,50)"))
  } else {
    fft_wl_formants <- fft_formants[1]
    fft_ov_formants <- fft_formants[2]
    if (fft_ov_formants < 0 || fft_ov_formants >= 100) {
      stop(glue("fft_ov_formants={fft_ov_formants} should be a percentage < 100"))
    }
    ## should also test if fft_wl_analysis is a multiple of 256?
  }

  #####-------------
  ## COMPUTE
  ## initialize "error"
  error <- ""

  ## initialise list failed
  failed <- data.frame()

  ## initialise "arguments"
  arguments <- data.frame(origin, min_freq_formants, max_freq_formants,
                          fft_wl_formants, fft_ov_formants, nb_formants)

  ## initialise formants columns
  f_cols <- paste("F", seq(1, nb_formants), ".upper", sep = "")
  arguments[f_cols] <- NA

  ## compute meanspec
  tryCatch(
    {
      meanspec_norm_chunk <- as.data.frame(
        meanspec(chunk, chunk@samp.rate,
                 wl = fft_wl_formants, ovlp = fft_ov_formants,
                 flim = c(min_freq_formants/1000, max_freq_formants/1000),
                 alim = c(0, 1), norm = T, plot = F
        )
      )
    },
    error = function(cond) {
      error <<- 'failed meanspec by get_formants_limits()'
      catn(error)
      catn(cond)
      catn("Meanspec input:")
      print(str(chunk))
      print(glue("{chunk@samp.rate};
                 wl: {fft_wl_formants}; ovlp: {fft_ov_formants};
                 flim: {min_freq_formants/1000} -> {max_freq_formants/1000};
                 alim: 0,1 ; norm: T"))
    }
  )

  if (error != ""){
    catn('File skipped')
    catn("File stored in a 'failed' dataframe")
    failed <- data.frame(origin)
    failed['error'] <- error
    out <- list(arguments, failed)
    names(out) <- c('formants_limits', 'failed_chunk')
    return(out)
  }

  meanspec_crop <- meanspec_norm_chunk[((meanspec_norm_chunk$x <= max_freq_formants/1000)
                                        & (meanspec_norm_chunk$x >= min_freq_formants/1000)),]

  ## find peaks
  tryCatch(
    {
      fpeaks_chunk <- data.frame(
        fpeaks(as.matrix(meanspec_crop), f = chunk@samp.rate,
               xlim = c(min_freq_formants/1000, max_freq_formants/1000),
               nmax = nb_formants, plot = show_steps())
      )
      names(fpeaks_chunk) <- c('freq', 'amp')
      fpeaks_chunk <- fpeaks_chunk[complete.cases(fpeaks_chunk),]

    },
    error = function(cond) {
      error <<- 'peaks not found by get_formants_limits()'
      catn(cond)
      catn("fpeaks input:")
      head(as.matrix(meanspec_crop))
      print(glue("Matrix dim: {dim(as.matrix(meanspec_crop))}"))
      print(glue("f: {chunk@samp.rate} ;
               xlim: {min_freq_formants/1000} -> {max_freq_formants/1000} ;
               nmax: {nb_formants}"))
    }
  )

  if (error != ""){
    catn('File skipped')
    catn(error)
    catn("File stored in a 'failed' dataframe")
    failed <- data.frame(origin)
    failed['error'] <- error
    out <- list(arguments, failed)
    names(out) <- c('formants_limits', 'failed_chunk')
    return(out)
  }

  if (show_steps()) {
    catn(glue("Searching for N={nb_formants} formants bands with get_formants_limits_chunk(), from {min_freq_formants} to{max_freq_formants} Hz, fft_wl_formants={fft_wl_formants}pts, fft_ov_formants={fft_ov_formants}% "))
    catn(glue("Computing meanspec with fft window length={fft_wl_formants}pts and ovlp={fft_ov_formants}%"))
    catn(glue("Searching for maximum {nb_formants} peaks on meanspec using fpeaks()"))
    pause()
  }


  up_bound <- vector(mode = "numeric", length = nb_formants)
  up_bound[nb_formants] <- max_freq_formants

  n_found_formants <- nrow(fpeaks_chunk)
  n_missing_formants <- nb_formants - n_found_formants

  # Keep the found peaks as the first formants
  expanded_formants <- vector(mode = "numeric", length = nb_formants)
  expanded_formants[seq(1, n_found_formants)] <- 1000 * fpeaks_chunk$freq

  # Add "fake" missing formants if any
  if (n_missing_formants > 0) {
    f_split <- (max_freq_formants - expanded_formants[n_found_formants]) / (2 * n_missing_formants + 1)
    for (i_miss_f in seq(1, n_missing_formants)) {
      expanded_formants[n_found_formants + i_miss_f] <- expanded_formants[n_found_formants] + 2 * i_miss_f * f_split
    }
  }

  # Compute upper bounds
  for (i_formant in seq(1, nb_formants - 1)) {
    up_bound[i_formant] <- .5 * (expanded_formants[i_formant]
                                 + expanded_formants[i_formant + 1])
  }


  ## plots limits and export
  if (show_steps()) {
    par(new = T)
    abline(v = up_bound/1000, col = 'red')
    par(new = F)
    catn(glue("Showing the expected formants limits based on detected peaks"))
    pause()
  }

  ## save in table
  arguments[f_cols] <- up_bound
  arguments['n_peaks'] <- n_found_formants


  if (show_steps()) {
    catn(glue("Showing the output table"))
    print(arguments)
    pause()
  }

  ## return boundaries
  out <- list(formants_limits = arguments,
              failed_chunk = failed)
  return(out)
}





#' Get formants limits - one file
#'
#' Search for expected formant limits in chunk, based on main peak on the measnpectrum.
#'
#' @param sound_files_path `chr` -- absolute path to the directory where the
#'                                  audio files from which the formants limits
#'                                  will be computed from are stored.
#' @param wav_file_name `chr` -- name of wave file in `sound_files_path`
#' @param nb_formants `num` -- number of expected formants
#' @param formants_bw -- formants bandwidth, a vector of length 2
#'                                  with minimum and maximum frequency of expected
#'                                  formants (Hz). Default is c(0,10000)
#' @param fft_formants -- a vector of length 2 with FFT window length (points)
#'                                  and overlap between windows (%).
#'                                  Default is c(256,50)
#' @param smoothing -- envelop smoothing a vector of length 2 with length of
#'                                  sliding window (points) and overlap between windows (%).
#'                                  Default is c(500,90)
#' @param min_dur `num` -- minimum duration (s) used for chunk detection
#'                                   in file. Default is 0.05s
#' @param amp_th `num` -- threshold (%) used for chunk detection. Default is 5%
#'
#'
#' @details
#' Uses timer() to detect the chunk in the sound, based on envelop `smoothing`
#' parameters, `min_dur` for chunk detection and amplitude threshold `amp_th`.
#' Detect chunk in a frequency range of interest `formants_bw` using ffilter().
#' Uses [get_formants_limits_chunk()] to extract expected formants limits on the
#' detected chunk in file.
#' May be used for computed several files in the directory. See [get_formants_limits_dir()]
#'
#' @return
#' A list of two elements:
#' 1. dataframe with the boundaries of expected
#' formants per chunk and on all computed files (one row per file).
#' 2. dataframe log of failed chunks and error type
#'
#' @export
#'
#'
#' @seealso [get_formants_limits_chunk()] [get_formants_limits_dir()]
get_formants_limits_file <- function(sound_files_path, wav_file_name,
                                     nb_formants,
                                     formants_bw = c(0,10000),
                                     fft_formants = c(256,50),
                                     smoothing = c(500,90),
                                     min_dur = 0.05, amp_th = 5) {

  #####-------------
  ## CONTROLS
  ## deal with frequency bandwidth for detection
  if (!is.vector(formants_bw)||!is.numeric(formants_bw)) {
    stop(glue("formants_bw={formants_bw} should be a numeric vector of length 2: min and max frequency in Hz. eg. freq = c(150,10000)"))
  } else {
    min_freq_formants <- formants_bw[1]
    max_freq_formants <- formants_bw[2]
    if (max_freq_formants < min_freq_formants){
      stop(glue("max formants frequency={max_freq_formants} should be greater than min formants frequency={min_freq_formants}"))
    }
  }

  ## deal with fft_formants
  if (!is.vector(fft_formants)||!is.numeric(fft_formants)) {
    stop(glue("fft_formants should be a numeric vector of length 2: c(fft_wl_formants, fft_ov_formants) (e.g. fft_analysis = c(512,50)"))
  } else {
    fft_wl_formants <- fft_formants[1]
    fft_ov_formants <- fft_formants[2]
    if (fft_ov_formants < 0 || fft_ov_formants >= 100) {
      stop(glue("fft_ov_formants={fft_ov_formants} should be a percentage < 100"))
    }
    ## should also test if fft_wl_analysis is a multiple of 256?
  }

  ## deal with smoothing
  env_wl <- smoothing[1]
  env_ov <- smoothing[2]
  if (!is.vector(smoothing) || !is.numeric(smoothing)) {
    stop(glue("smoothing should be a numeric vector of length 2: length of window = env_wl (points) and overlap = env_ov (%) (eg. smoothing = c(500,90)"))
  }else{
    if (env_ov < 0 || env_ov >= 100) {
      stop(glue("env_ov={env_ov} should be a percentage < 100"))
    }
  }

  if(!is.numeric(min_dur)) {
    stop(glue("min_dur={min_dur} should be numerical"))
  }

  if(!is.numeric(amp_th)) {
    stop(glue("amp_th={amp_th} should be numerical"))
  } else {
    if (amp_th < 0 || amp_th >= 100) {
      stop(glue("amp_th={amp_th} should be a percentage < 100"))
    }
  }

  #####---------------
  ## COMMPUTE
  ## initialize "error"
  error <- ""

  ## get origin
  origin <- tools::file_path_sans_ext(wav_file_name)

  ## initialise list failed
  failed <- data.frame()

  ## get wav_file
  wav_file <- file.path(sound_files_path, wav_file_name)

  ## load sound and channel
  sound <- readWave(wav_file, from = 0, to = Inf, units = "seconds")

  ## initialise "arguments"
  arguments <- data.frame(origin, min_freq_formants, max_freq_formants,
                          fft_wl_formants, fft_ov_formants, nb_formants,
                          env_wl, env_ov, min_dur, amp_th)



  if (active_mono_left()){
    sound <- mono(sound, "left")
    channel <- 'left'
  } else{
    sound <- mono(sound, "right")
    channel <- 'right'

  }

  ## detect chunk in sound
  sound <- addsilw(sound, sound@samp.rate, at = "end", d = 0.01, plot = F,
                   output = "Wave")
  sound <- addsilw(sound, sound@samp.rate, at = "start", d = 0.01, plot = F,
                   output = "Wave")

  sound_filtered <- ffilter(sound,custom = NULL,from = min_freq_formants,
                            to = max_freq_formants, wl = get_fft_wdw()$len, ovlp =  get_fft_wdw()$ovlp,
                            wn = "hanning", fftw = FALSE,
                            output="Wave")

  ## detect sound based on amp_th
  tryCatch(
    {
      ## detect call in sound to compute duration
      detect <- timer(sound_filtered, f = sound_filtered@samp.rate,
                      threshold = amp_th, dmin = min_dur,
                      power = 2, msmooth = c(env_wl,env_ov),
                      plot = show_steps(), main = paste('Detection of chunk:', origin))

      chunk_dur <- max(detect$s)
      real_start_chunk <- detect$s.start[which(detect$s == chunk_dur)] - 0.01 # remove security added with addsil
      real_end_chunk <- real_start_chunk + chunk_dur

      ## load sound and channel with real start and end
      chunk <- readWave(wav_file, from = real_start_chunk,
                        to = real_end_chunk, units = "seconds")

      if (active_mono_left()) {
        chunk <- mono(chunk, "left")
        channel <- 'left'
      } else {
        chunk <- mono(chunk, "right")
        channel <- 'right'
      }
    },
    error = function(cond) {
      print("alll")
      error <<- 'sound not detected by timer'
      catn("Timer input:")
      print(str(sound_filtered))
      print(glue("f: {sound_filtered@samp.rate}, threshold: {amp_th},
                  dmin: {min_dur}, power: 2, msmooth: {env_wl}->{env_ov}"))
    }
  )

  if (error != ""){
    catn('File skipped')
    catn(error)
    catn("File stored in a 'failed' dataframe")
    failed <- data.frame(origin)
    failed['error'] <- error
    out <- list(arguments, failed)
    names(out) <- c('formants_limits', 'failed_chunk')
    return(out)
  }

  if (show_steps()) {
    catn("Detecting chunk limits and extract:")
    catn(glue("Sound bandbpass filtered by ffilter() from {min_freq_formants} to {max_freq_formants} Hz, using FFT wn = 'hanning' window type and global variables fft_wl={get_fft_wdw()$len} pts and fft_ov={get_fft_wdw()$ovlp} %.
    Chunk detected by timer() based on envelop smoothing=c({env_wl},{env_ov}) amp_th={amp_th}, min_dur={min_dur}, power = 2"))
    pause()
  }

  tryCatch(
    {
      ## filter chunk
      chunk <- ffilter(chunk, from = min_freq_formants, to = max_freq_formants,
                       wl = get_fft_wdw()$len, ovlp = get_fft_wdw()$ovlp, wn = "hanning", output="Wave")


      res_formants_limits_chunk <- get_formants_limits_chunk(chunk = chunk, origin = origin,
                                                             formants_bw = formants_bw,
                                                             fft_formants = fft_formants,
                                                             nb_formants = nb_formants)
    },
    error = function(cond) {
      error <<- 'could not find formant limits in chunk'
    }
  )


  if (error != ""){
    catn('File skipped')
    catn(error)
    catn("File stored in a 'failed' dataframe")
    failed <- data.frame(origin)
    failed['error'] <- error
    res_formants_limits <- arguments
  } else {
    res_formants_limits <- merge(res_formants_limits_chunk$formants_limits,
                                 arguments,
                                 by=intersect(names(res_formants_limits_chunk$formants_limits),
                                              names(arguments)))
    if (nrow(res_formants_limits) != nrow(res_formants_limits_chunk$formants_limits)) {
      catn("Warning: when aggregating formants, not the same number of rows
           between chunk and file")
      catn("Merge: ")
      print(res_formants_limits_chunk$formants_limits)
      catn("and:")
      print(arguments)
      catn("should have gone smoothly, but got:")
      print(res_formants_limits)
    }
  }

  out <- list(
    formants_limits = res_formants_limits,
    failed_chunks = failed
  )

  return(out)
}




#' Get formants limits - directory
#'
#' @param sound_files_path `chr` -- absolute path to the directory where the
#'                                  audio files from which the formants limits
#'                                  will be computed from are stored.
#' @param nb_formants `num` -- number of expected formants
#' @param list_of_files -- a character vector containing the origin names
#'                                  of the wave files to use. If NULL, all
#'                                  wave from `sound_files_path` will be read.
#' @param dst_path `chr` -- absolute path to the directory where the
#'                                  the results should be stroed. Default is NULL.
#'                                  If default, `sound_files_path` is used.
#' @param formants_bw -- formants bandwidth, a vector of length 2
#'                                  with minimum and maximum frequency of expected
#'                                  formants (Hz). Default is c(0,10000)
#' @param fft_formants -- a vector of length 2 with FFT window length (points)
#'                                  and overlap between windows (%).
#'                                  Default is c(256,50).
#' @param smoothing -- envelop smoothing a vector of length 2 with length of
#'                                  sliding window (points) and overlap between windows (%).
#'                                  Default is c(500,90)
#' @param min_dur `num` -- minimum duration (s) used for chunk detection
#'                                   in file. Default is 0.05s
#' @param amp_th `num` -- threshold (%) used for chunk detection. Default is 5
#'
#'
#' @details
#' Applies get_formant_limits_file() on all selected files of the `sound_files_path`
#' directory.
#'
#'
#' @return
#' A list of two elements: 1. dataframe with the boundaries of expected
#' formants per chunk and on all computed files (one row per file). 2. dataframe log of failed chunks and error type
#'
#'
#' @export
#' @seealso [get_formants_limits_chunk()], [get_formants_limits_file()]
#'
get_formants_limits_dir <- function(sound_files_path, nb_formants,
                                    list_of_files = NULL, dst_path = NULL,
                                    formants_bw = c(0,10000),
                                    fft_formants = c(256,50),
                                    smoothing = c(500,90),
                                    min_dur = 0.05, amp_th = 5) {
  #####-----------------
  ## CONTROLS

  ## dst path
  if (is.null(dst_path)) {
    dst_path <- sound_files_path
  } else {
    if (!dir.exists(dst_path)) {
      catn("dst_path did not exist - created")
      dir.create(dst_path)
    }
  }

  ## deal with fft_formants
  if (!is.vector(fft_formants)||!is.numeric(fft_formants)) {
    stop(glue("fft_formants should be a numeric vector of length 2: c(fft_wl_formants, fft_ov_formants) (e.g. fft_analysis = c(512,50)"))
  } else {
    fft_wl_formants <- fft_formants[1]
    fft_ov_formants <- fft_formants[2]
    if (fft_ov_formants < 0 || fft_ov_formants >= 100) {
      stop(glue("fft_ov_formants={fft_ov_formants} should be a percentage < 100"))
    }
    ## should also test if fft_wl is a multiple of 256?
  }

  ## deal with smoothing
  env_wl <- smoothing[1]
  env_ov <- smoothing[2]
  if (!is.vector(smoothing) || !is.numeric(smoothing)) {
    stop(glue("smoothing should be a numeric vector of length 2: length of window = env_wl (points) and overlap = env_ov (%) (eg. smoothing = c(500,90)"))
  }else{
    if (env_ov < 0 || env_ov >= 100) {
      stop(glue("env_ov={env_ov} should be a percentage < 100"))
    }
  }

  if(!is.numeric(min_dur)) {
    stop(glue("min_dur={min_dur} should be numerical"))
  }

  if(!is.numeric(amp_th)) {
    stop(glue("amp_th={amp_th} should be numerical"))
  } else {
    if (amp_th < 0 || amp_th >= 100) {
      stop(glue("amp_th={amp_th} should be a percentage < 100"))
    }
  }

  ## get the list_of_files
  if(is.null(list_of_files)) {
    list_of_files <- list.files(sound_files_path, get_wav_ext())
  } else {
    grep_wav <- grep(get_wav_ext(), list_of_files)
    if(length(grep_wav) == 0) {
      list_of_files <- paste(list_of_files, get_wav_ext(), sep='.')
    }
  }
  if(length(list_of_files) == 0) {
    stop("could not find the files in directory. Maybe check the wave extension get_wav_ext() and set_wav_ext()")
  }

  ## deal with frequency bandwidth for detection
  if (!is.vector(formants_bw)||!is.numeric(formants_bw)) {
    stop(glue("formants_bw={formants_bw} should be a numeric vector of length 2: min and max frequency in Hz. eg. freq = c(150,10000)"))
  } else {
    min_freq_formants <- formants_bw[1]
    max_freq_formants <- formants_bw[2]
    if (max_freq_formants < min_freq_formants){
      stop(glue("max formants frequency={max_freq_formants} should be greater than min formants frequency={min_freq_formants}"))
    }
  }

  #####----------------
  ## INIT

  ## keep info SHOW_STEPS
  show_steps_tmp <- show_steps()
  first_file <- TRUE

  ## initialize res_dir
  res_dir <- data.frame()

  ## initialize error_list
  error_list <- data.frame()

  #####---------------
  ## LOOP

  ## loop on all selected files
  tryCatch(
    {
      ## loop for all files
      for(file in list_of_files){

        if (show_steps()) {
          catn("Show steps On!")
          catn(glue("I will show the steps on the first file only"))
          pause()
        }

        formants_limits <- get_formants_limits_file(sound_files_path = sound_files_path,
                                                    nb_formants = nb_formants,
                                                    wav_file_name =  file,
                                                    formants_bw = formants_bw,
                                                    fft_formants = fft_formants,
                                                    smoothing = smoothing,
                                                    min_dur = min_dur, amp_th = amp_th)

        res_dir <- rbind(res_dir, formants_limits$formants_limits)
        error_list <- rbind(error_list, formants_limits$failed_chunk)

        ## deal with show_steps on first file only
        set_show_steps(FALSE)

      }
    },  error = function(cond){
      catn('ALERT ERROR! Program stopped')
      catn('Try to remove the file that stopped from the folder and run again')
      message(cond)
      pause()
    },
    finally = {
      set_show_steps(show_steps_tmp)
    }
  )

  res_dir_file_name <- paste('formants_limits_dir',nb_formants, fft_wl_formants, fft_ov_formants, sep = '.')
  res_dir_file_name <- paste(res_dir_file_name, '.tsv', sep = '')
  res_dir_file <- file.path(dst_path, res_dir_file_name)
  write.table(res_dir, res_dir_file, sep = '\t', row.names = F,
              quote = F)

  errors_file_name <- paste('formants_limits_errors',nb_formants, fft_wl_formants, fft_ov_formants, sep = '.')
  errors_file_name <- paste(errors_file_name, '.tsv', sep = '')
  errors_file <- file.path(dst_path, errors_file_name)
  write.table(error_list, errors_file, sep = '\t', row.names = F,
              quote = F)

  out <- list(res_dir, error_list)
  names(out) <- c('formants_limits_dir', 'formants_limits_errors')
  catn(glue("Result dataframe of get formant limits saved in {dst_path},
            tab separated value file, named {res_dir_file_name}.
            Log of failed chunks saved in {dst_path},
            tab separated values file, named {errors_file_name}"))
  return(out)
}
