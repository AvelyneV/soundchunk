Package: soundchunk
Type: Package
Title: Management, Extraction and Analysis of Sounds of Interest
Version: 1.0.0
Authors@R: c(
    person("Avelyne S.", "Villain", role = c("cre", "aut", "ctb", "cph"),
           email = "avelyne.s.villain@protonmail.com"),
    person("Paul", "Renaud-Goud", role = c("aut", "ctb", "cph"),
           email = "prenaud@irit.fr")
    )
Author: Avelyne S. Villain [cre, aut, ctb, cph], Paul Renaud-Goud [aut, ctb, cph]
Maintainer: Avelyne S. Villain <avelyne.s.villain@protonmail.com>
Description: A set of user-oriented tools to manage, extract and analyse sounds.
    Chop sound files into sounds of interest, store them in a structured way
    along with meta-data and provide acoustic analysis methods. See Avelyne S.
    Villain, Paul Renaud-Goud (2023), <doi:10.1101/2023.08.29.555312>.
License: GPL-3 + file LICENSE
Encoding: UTF-8
LazyData: true
RoxygenNote: 7.2.3
Roxygen: list(markdown = TRUE)
Suggests:
    knitr,
    rmarkdown,
    spelling
VignetteBuilder:
  knitr
Imports:
  rjson,
  seewave,
  tuneR,
  stringi,
  stringr,
  glue,
  tools,
  graphics,
  dplyr,
  utils,
  phonTools,
  grDevices,
  stats,
  rlang
Language: en-GB
