* TODO To version 1.0.0 [53/54]
- [X] un seul point d'entrée pour les fonctions sur les répertoires ou
  les fichiers ? **NON**
- [X] extension wave : ce serait bien de ne pas demander et de
  détecter (au pire, mettre une variable dans l'environnement pour
  choisir l'extension si on doit écrire un wave (mais on part toujours
  d'un wave existant anyway, non ?)) **NON** **SI**
- [X] split =segmentation_parameters_file= function into several
- [X] factoriser les readline
- [X] generaliser chop recording to no lab_ext
- [X] deal with mono stereo in dispatch,
- [X] deal with mono stereo interactive_chunks_detection,
- [X] deal with mono stereo auto_chunking
- [X] split detect_chunks
- [X] =interactive_chunk_detection=: listen
- [X] chop per phase instead of start/stop
- [X] virer les -1 dans l'initialisation d'auto-chunk
- [X] préfixe dans nom de répertoire cible de flat, et dans tsv. NON
- [X] vérifier les listages de fichiers ; NE PAS utiliser
      #+begin_src R
        list.files(path=pouet, pattern=".txt\\>")
      #+end_src
      utiliser
      #+begin_src R
        list.files(path=pouet, pattern="\\.txt$")
      #+end_src
- [X] demander confirmation après print env du package
- [X] detect_chunks_default_label: TODO add warning file too long.
- [X] gerer timer issue... (important pour auto_chunking)
- [X] tester norm avant timer
- [X] workflow
  - [X] naming_rule: dire que les info recording sont dans un
    parameters.tsv + get_naming_rule
- [X] expand label content
- [X] auto_chunking_dir
- [X] norm_value norm
- [X] spectro setting --> global variables
- [X] check for a note about FFT window length in functions
  - [X] dispatch (file dir, full flat)
  - [X] interactive_chunks_detection (file dir)
  - [X] auto_chunking setting see spectr() arguments
- [X] doc and lint [11/11]
  - [X] audacity.R
  - [X] auto_chunking.R
    - [X] auto_chunking slice doc + careful `norm` instead of `norm_value`
  - [X] chop_recording.R
  - [X] detect_chunks_default_label.R
  - [X] dispatch_chunks_flat.R
  - [X] dispatch_chunks_full.R
  - [X] dispatch_chunks.R
  - [X] expand.R
  - [X] global.R
  - [X] hello.R
  - [X] helpers.R
- [X] interactive_chunking.R [9/9]
  - [X] add note about spectro settings
  - [X] bug listen
  - [X] bug background
  - [X] bug when entering tune setting but not tuning
  - [X] ask false pos/false neg after quitting a file instead
  - [X] put previous final settings of tuning in default settings if tune again
  - [X] say norm=FALSE => nom_value=0
  - [X] naming.R
  - [X] prompts.R
- [X] trycatch [2/2]
  - [X] remove warnings
  - [X] all here?
- [X] alert 10s
- [X] checker les logs : retour, fichier [2/2]
  - [X] grouped_db_parameters_dir in dispatch_chunks_grouped
  - [X] nothing/something in dispatch_chunks_grouped_file --> import from flat
- [X] glue issues
- [X] show_steps try catch for chopping_recordings
- [X] merge clean_audacity and read_like_audacity **NON**
- [X] acceptable answers set
- [X] wave does not exit : list WAV and propose
- [X] harmoniser les noms
- [X] =origin=: c'est clair ?
- [X] plot_label plot_no_label 810 interactive
- [X] check the TODOs
- [-] auto-chunk [2/3]
  - [ ] virer les sécurités pour merger dans auto-chunk
  - [X] regarder security NA
  - [X] auto_chunking --> channel right issue // could not replicate
- [X] vocabulary [3/3]
  - [X] parameters of the recording -> context ?
  - [X] auto_chunking, interactive_chunking -> chunk_auto, chunk_interactive
  - [X] slice/fragment
- [X] catn de "working on file ... : "cleaning audacity file: ,
  dispatching file:, interactively chunking
  file:, expanding labels file: , chopping file:
- [X] detect_cunks_interactive() ajouter ligne vide avant "Possible
  actions:" ??
- [X] semimanual --> chunks.txt
- [X] doc detect_chunks() "num or NULL – an envelop value that is be
  used to normalize every slice TODO
- [X] doc detect_chunks() ajouter que la norm_value, on l'a dans le
  log the detect_chunks_interactive()
- [X] workflow test [12/12]
  - [X] ajout ?package
  - [X] group vide naming
  - [X] line vide avant parameters for groups
  - [X] dispatch : assign variable to avoid printing the flat_database_parameter
  - [X] Generate fragments of 10 secs.
  - [X] active phase screenchot
  - [X] parameters.tsv screenshots
  - [X] worklow variable chop_recordings
  - [X] set_fast_display(TRUE) --> set_fast_display_spectro(TRUE)
  - [X] security = 5 --> 10
  - [X] "extract.txt" --> mettre dans une variable si ya pas de
    'auto.ed.txt'
  - [X] This PDF document is available as a Rnotebook here:
- [X] data repository !
- [X] relesae email
- [X] release email list ABORTED
- [X] min_freq max_freq units
- [X] clean normalisation
- [X] quand on a fait un peu de tuning et qu'on a arrêté et qu'on
  revient dessus et qu'on skip. Il recalcule la norm value sur les
  fichiers restant. Donc quand le log exist, il faudrait aller
  chercher la norm value du log.

- [X] ça serait bien de sauver quelque part tous les calculs de max
  env qu'on run sur les fichiers de tuning, ainsi on peut plus se
  rendre compte de cet norm value et si le max par example, c'est
  representatif que de 1 fichier ou alors plusieurs...

- [X] auto chunk : dans les auto.txt les index commencent à 2?


* TODO To version 1.1.0
- [X] change =scnf-data= entry in =references.bib= PRG
- [ ] add get_env_bins in workflow AV
- [X] expand_labels() --> make it a global variable stored so that we
  can get it if needed without having to redo ? cf: set_naming_rule_path() PRG
- [X] showsteps --> dans clean_audacity() ya pas que je premier fichier PRG
- [X] je continue à penser qu'il faut une norm value moyenne et pas le
  max. ou alors on regarde la distribution des env max et on prend un
  Q quelque chose? Q80?
- [X] dans dispatch ya deux columns origin ducoup dans le ouput
- [ ] chunk_origin?? parameters AV
- [-] split vingettes
  - [X] package env
  - [X] generate sound database of labeled data
  - [ ] detect chunks in non labeled data
  - [X] acoustic analysis
- [ ] dispatch
  - [ ] [fix] error dispatch when label track is empty --> skip and show
    message [PRG]
  - [ ] delete content of a non-empty directory
  - [ ] remove flat
- [ ] set_naming rule: ask user whehter or not to save + ask path to
  dir + ask for name of file + print the absolut path [PRG]
- [X] [fix] expand label when typo in label --> change message to clearer one [PRG]
- [ ] [dev] get SNR : add rms of chunk in full data [AV]
- [ ] [doc] preprint: add extract_chunk_features

** Release email

Dear colleagues,

We introduce SoundChunk: A free open-source and user
oriented R package for sound chunking.

The work is off course still in progress but we believe this version
is stable enough to be confronted to a variety of acoustic data and
users. We aim at developping functions and workflows that would fit
most of us, we would be thus be grateful to have any feeback,
suggestions, or contributions.


*Motivation:*

In a standard bioacoustic experiment setting, after data collection
and before data analysis lies a time-consuming process that consists
in extracting and labeling sounds of interest (chunks) from usually
long recording ﬁles, collected through spreading PAM (Passive Acoustic
Monitoring) for instance, and storing them in a structured and
exploitable way based on both the meta-data of the initial recordings
and the label data.

SoundChunk is a
user-oriented package in R that aims at providing tools for any R user
so that they can go efficiently and comfortably through this
process. Usually, the tasks include detecting, labeling, and
extracting sound from audio files. With the recent advance of Machine
Learning, especially Convolutional Neural Networks, some of these
tasks are integrated in the ML framework; however, the learning phase
relies on labeled chunks that need to be created.

*Content of V1.0.0:*

This version contains functionalities regarding the chunking of mono
(for now) recordings and building of modular sound
databases. Functionalities regarding features extraction and global
assessment of acoustic data will come later to complement existing R
package that already do it (Seewave and Soundgen as examples).

*Discover `SoundChunk`:*
1. Using three lines of code in R...
   - Install the package:
     remotes::install_gitlab("AvelyneV/soundchunk", host =
     "framagit.org", dependencies = TRUE, build_vignettes = TRUE)
   - Load the library: library(soundchunk)
   - Access a demo workflow: vignette("workflow", package =
     "soundchunk")
2. ... and a set of example data:
   https://zenodo.org/record/8010267
3. Or through the preprint: XXX

Contact and/or join us: soundchunknfeat@proton.me

Please share to anybody who might be interested.

In case you use the package, please cite the preprint.

All the best,
Avelyne Villain & Paul Renaud-Goud.

** email list

elodie.briefer@bio.ku.dk, clementine.vignal@sorbonne-universite.fr>,
Celine Tallet <celine.tallet@inrae.fr>, mathevon@univ-st-etienne.fr,
frederic.sebe@univ-st-etienne.fr, contact@biophonia.fr, virgile.daunay
<virgile.daunay@ik.me>

- behavioural ecology group:
Xueying Zhu <jzp874@alumni.ku.dk>, Heidi Maria Thomsen
<hmthomsen@bio.ku.dk>, Osiecka Anna <ann.osiecka@gmail.com>, Avelyne
Sylvie Marie Villain <avelyne.villain@bio.ku.dk>, Karina Stampe Ernst
<karina.stampe@bio.ku.dk>, Cristina Fernández García
<bmk694@alumni.ku.dk>, jacopo di clemente
<jacopo.diclemente@outlook.com>, Romain Adrien Lefevre
<romain.lefevre@bio.ku.dk>, Malene Friis Hansen
<malenefriishansen@gmail.com>, Damaris Coralie Riedner
<damaris.riedner@bio.ku.dk>, Tine Simonsen <tsimonsen@bio.ku.dk>,
Elodie Floriane Mandel-Briefer <elodie.briefer@bio.ku.dk>, Sabrina
Engesser <sab.engesser@gmail.com>, Sixue Li <klv859@alumni.ku.dk>,
Sarah Falck Jensen <201808929@post.au.dk>, Mathia Gerner
<csf792@alumni.ku.dk>, Athanasia Alexiadou <pdg296@alumni.ku.dk>,
Daniel Rueskov Nielsen <Krejmar@outlook.com>, prenaud@irit.fr
<prenaud@irit.fr>, Akvile Galeckaite <bvd918@alumni.ku.dk>, Hannah
Juul Michaelsen <hlg495@alumni.ku.dk>, Zoé Miot
<nc.zoemiot@gmail.com>, stefania.celozzi@unimi.it
<stefania.celozzi@unimi.it>, Dina Abdelhafez Ali Mostafa
<a12043275@unet.univie.ac.at>, Cornelia Hindhede Warrer
<zrs678@alumni.ku.dk>, Pernille Glææsner Kildebo-Jensen
<hsf642@alumni.ku.dk>, Alice Fratesi <hlv870@alumni.ku.dk>, Giovanni
Leonardi <xlc856@alumni.ku.dk>, Drew Lincoln Larson
<dnw282@alumni.ku.dk>, Dido Sotiropoulou
<didw_swtiropoulou@hotmail.com>, Nadja Vive Ivø Beier
<vbl485@alumni.ku.dk>, Laura Julie Purlund <vtp642@alumni.ku.dk>


* TODO To version 2.1.0
- [ ] normalisation --> detect_chunk_intereactive should ask for a norm value
- [X] depedencies --> fftw package PRG
- [ ]

* TODO To version 2.2.0
- [X] get_meanspec()
- [X] magic_plot_spec() magic_plot_env()
- [-] get_chunk_features()
  - [X] freq distrib
  - [X] duration
  - [X] dfreq
  - [X] fund
  - [-] formants
    - [X] bands
    - [ ] LPC
  - [ ] amp mod
- [X] get_formant_limits
  - [X] original
  - [X] upgraded
- [X] detect_chunk_limit
- [X] get_formants
- [X] get_snr()
- [X] spectrograms
- [X] add vignette chunks features
- [X] package environ. print_pkg_environment.
- [ ] vignettes: fix path and extension: inspection + analysis
- [ ] add get_rms() [AV]

* TODO To Version 2.3.0
- [ ] match filter before timer to play with the noisiness before
  timer detection --> see Sueur 2018?
- [ ] on ultrasonic --> play must be sĺowed down
- [ ] add detect_source
- [ ] for dispatch (full) --> maybe change a bit so that we can create
  modular databases with only few info of the recording but still have
  all chunks saved.

* Remarques
- =match= pour l'index d'un élément dans un vecteur
- interactive_chunks_detection --> normalisation computed using
  smoothing parameters of env()

* Bug
#+begin_src R
  detect_chunks_interactive_file(src_path = "/home/poulpos/data/scnf", dst_path = "/home/poulpos/data/scnf", wav_file = "test.wav", min_freq = 0, max_freq = 8000, threshold = 10, power = 2, env_window_length = 512, env_window_ov = 10, min_dur = 0.05, accepted_labels = c("aze"))
  detect_chunks_auto_file(src_path = "/home/poulpos/data/scnf", origin = "test",  dst_path = "/home/poulpos/data/scnf", slice_dur = 1.5, slice_ov = .1, min_freq = 0, max_freq = 8000, threshold = 10, power = 2, env_window_length = 512, env_window_ov = 10, min_dur = 0.05, default_label = "def", security = 0.05, norm_value = 409.8752)
#+end_src

#+begin_src R
  norm_env <- data.frame(norm_value=c(), max_env=c())
  for (norm_value in seq(100, 5000, 100)) {
    s <- synth(f = 44100,
      d = 1,
      cf = 4000,
      a = norm_value, output = "Wave")
      max_env <- max(env(s, f = s@samp.rate, envt = "abs",
             msmooth = c(1500, 90),
             norm = FALSE, plot = FALSE))
      norm_env <- rbind(norm_env, data.frame(norm_value, max_env))
    }
  print(norm_env)
#+end_src

#+begin_src R
  src_path = "/home/poulpos/data/scnf/2_non_labeled_data/raw"
  origin = "221.22.11.08"

  timer_runtime <- data.frame(slice=c(), run=c())
  for (slice in c(120, 60, 45, 30)) {
    catn("slice")
    print(slice)
  start.time <- Sys.time()
  detect_chunks_auto_file(src_path = src_path, origin = origin,
                          dst_path = src_path, slice_dur = slice, slice_ov = .1,
                          min_freq = 0, max_freq = 8000, threshold = 10,
                          power = 2, env_window_length = 512,
                          env_window_ov = 10, min_dur = 0.06,
                          default_label = "def", security = 5,
                          norm_value = NULL)
    end.time <- Sys.time()
    run <- end.time - start.time
      timer_runtime <- rbind(timer_runtime, data.frame(run, slice))
  }
#+end_src


* CRAN
- [[https://cran.r-project.org/web/packages/submission_checklist.html][checklist from cran]]
- [[https://kbroman.org/pkg_primer/pages/cran.html][advices]]



** from usethis

[[https://r-pkgs.org/release.html][explained]]

*** First release [2/8]

- [X] `usethis::use_news_md()`
- [X] `usethis::use_cran_comments()`
- [ ] Update (aspirational) install instructions in README
  - [ ] improve and proofread
  - [ ] check vignettes
- [ ] Proofread `Title:` and `Description:`
  - [ ] mail address?
- [ ] Check that all exported functions have `@return` and `@examples`
- [ ] Check that `Authors@R:` includes a copyright holder (role 'cph')
- [ ] Check [licensing of included files](https://r-pkgs.org/license.html#sec-code-you-bundle)
- [ ] Review <https://github.com/DavisVaughan/extrachecks>

*** Prepare for release [0/7]

- [ ] `git pull`
- [ ] `usethis::use_github_links()`
- [ ] `urlchecker::url_check()`
- [ ] `devtools::check(remote = TRUE, manual = TRUE)`
- [ ] `devtools::check_win_devel()`
- [ ] `git push`
- [ ] Draft blog post

*** Submit to CRAN [0/3]

- [ ] `usethis::use_version('minor')`
- [ ] `devtools::submit_cran()`
- [ ] Approve email

*** Wait for CRAN [0/7]

- [ ] Accepted :tada:
- [ ] Add preemptive link to blog post in pkgdown news menu
- [ ] `usethis::use_github_release()`
- [ ] `usethis::use_dev_version(push = TRUE)`
- [ ] `usethis::use_news_md()`
- [ ] Finish blog post
- [ ] Tweet




* CI
- [[https://gitlab.paca.inrae.fr/r-ecosystem/cookbooks/r-packages-ci-cd][INRA]]
- [[https://www.r-bloggers.com/2018/08/developing-r-packages-with-usethis-and-gitlab-ci-part-i/][blog]]


1200,     954,     468 -> 672, 350
384, 1298,910,954,
