## R CMD check results

0 errors | 0 warnings | 1 note

* This is a new release.
* One NOTE remaining, we guess because we changed Maintainer's info. We actually
  wanted to have the "cre" role on the author Avelyne Villain, but a
  collective maintenance (behind the soundchunknfeat@protonmail.com address) (in
  the field Maintainer), but it raised an error on check.
